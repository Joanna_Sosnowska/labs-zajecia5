﻿using System;
using System.Reflection;
using PK.Container;
using Wyswietl.Implementation;
using Wyswietl.Contract;
using Glowny.Implementation;
using Glowny.Contract;


namespace Lab5.Infrastructure
{
    public struct LabDescriptor
    {
        #region P1

        public static Type Container = typeof(PK.Container.IContainer);

        #endregion

        #region P2

        public static Assembly MainComponentSpec = Assembly.GetAssembly(typeof(Glowny.Contract.IGlowny));
        public static Assembly MainComponentImpl = Assembly.GetAssembly(typeof(Glowny.Implementation.Glowny));

        public static Assembly DisplayComponentSpec = Assembly.GetAssembly(typeof(Wyswietl.Contract.IWyswietl));
        public static Assembly DisplayComponentImpl = Assembly.GetAssembly(typeof(Wyswietl.Implementation.Wyswietl));

        #endregion
    }
}
