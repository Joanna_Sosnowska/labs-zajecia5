﻿using System;
using PK.Container;
using Wyswietl.Implementation;

namespace Lab5.Infrastructure
{
    public class Configuration
    {
        /// <summary>
        /// Konfiguruje komponenty używane w aplikacji
        /// </summary>
        /// <returns>Kontener ze zdefiniowanymi komponentami</returns>
        public static IContainer ConfigureApp()
        {
            var container = new PK.Container.Container();
           
            var c1 = container as IContainer;
            return c1;
           
        }
    }
}
