﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wyswietl.Contract
{
    public interface IWyswietl
    {
        void Wlacz();
        void Wylacz();
    }
}
