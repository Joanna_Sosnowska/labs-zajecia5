﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wyswietl.Contract;
using Lab5.DisplayForm;

namespace Wyswietl.Implementation
{
    public class Wyswietl:IWyswietl
    {

        public void Wlacz()
        {
            Console.WriteLine("Wyswietl wlaczony");
           
        }

        public void Wylacz()
        {
            Console.WriteLine("Wyswietl wylaczony");
        }
        public override string ToString()
        {
            return "Wyswietlacz";
        }
    }
}
