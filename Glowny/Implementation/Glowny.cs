﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Glowny.Contract;
using Wyswietl.Implementation;

namespace Glowny.Implementation
{
    public class Glowny:IGlowny
    {
        
        public void GlownyWlacz()
        {
            Console.WriteLine("Glowny wlaczone");
        }

        public void GlownyWylacz()
        {
            Console.WriteLine("Glowny wylaczone");
        }
    }
}
